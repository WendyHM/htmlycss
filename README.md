# HTML y CSS Básico

En este repositorio se encuentran los ejercicios del curso básico de HTML y CSS.

## Descripción

Ejercicios básicos para construir contenido Web bien estructurado y fácil de mantener, pensando en el trabajo colaborativo. 

## Autor

* Hernández Méndez Wendy

### Contacto

wendy.hm005@ciencias.unam.mx

## Instructor

* Vargas Zermeño Edgar